package com.devpro.goolgeassistant;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.devpro.goolgeassistant.databinding.ActivityMainBinding;
import com.devpro.goolgeassistant.inter.OnResultsReady;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnResultsReady {
    private ActivityMainBinding binding;
    private SpeechRecognizerManager speechRecognizerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            checkPermission();
        }
        speechRecognizerManager = new SpeechRecognizerManager(this, this);
        binding.btnVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speechRecognizerManager.startListening();

            }
        });
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 100);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        speechRecognizerManager.stop();
    }

    @Override
    public void onResults(ArrayList<String> results) {
        Toast.makeText(this, results.get(0), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, results.get(0));
        startActivity(intent);
        if (speechRecognizerManager.ismIsListening()) {
            speechRecognizerManager.stopListening();
        }
    }

    @Override
    public void onStreamingResult(ArrayList<String> partialResults) {

    }
}