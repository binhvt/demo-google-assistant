package com.devpro.goolgeassistant;

import android.content.Context;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.widget.Toast;

import com.devpro.goolgeassistant.inter.OnResultsReady;

import java.util.ArrayList;

public class SpeechRecognitionListener implements RecognitionListener {
    private OnResultsReady mListener;
    private Context mContext;

    public SpeechRecognitionListener(OnResultsReady mListener, Context mContext) {
        this.mListener = mListener;
        this.mContext = mContext;
    }

    @Override
    public void onReadyForSpeech(Bundle bundle) {

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float v) {

    }

    @Override
    public void onBufferReceived(byte[] bytes) {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onError(int error) {
        if (error == SpeechRecognizer.ERROR_NETWORK) {
            ArrayList<String> errorList = new ArrayList<String>(1);
            errorList.add("STOPPED LISTENING");
            if (mListener != null) {
                mListener.onResults(errorList);
                Toast.makeText(mContext, "NETWORK ERROR", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onResults(Bundle results) {
        if (results != null && mListener != null) {
            ArrayList<String> ahihi = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            mListener.onResults(ahihi);
        }
    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        if (partialResults != null && mListener != null) {
            ArrayList<String> texts = partialResults.getStringArrayList("android.speech.extra.UNSTABLE_TEXT");
            mListener.onStreamingResult(texts);
        }
    }

    @Override
    public void onEvent(int i, Bundle bundle) {

    }
}
